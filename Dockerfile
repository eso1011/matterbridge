# Compile stage
FROM golang:1.13.1-alpine AS build-env

#copy source
COPY ./src /go/bin/matterbridge

#compile binary file (matterbridge)
WORKDIR /go/bin/matterbridge
RUN go build


# Final stage
FROM alpine

#copy binary file
COPY --from=build-env /go/bin/matterbridge/matterbridge /

#copy config for bridge
COPY --from=build-env /go/bin/matterbridge/matterbridge.toml /
#ENV http_proxy http://user:pass@address

# Run matterbridge
CMD ["/matterbridge", "--debug"]
